from random import randint

class Monde:
    def __init__(self, dimension, duree_repousse):
        """ constructeur du monde"""
        self.dimension = dimension
        self.duree_repousse = duree_repousse
        self.carte = [[randint(0,duree_repousse*2) for i in range(dimension)] for j in range(dimension)]

    def herbePousse(self):
        for i in range(len(self.carte)):
            for j in range(len(self.carte[i])):
                if self.carte[i][j] < self.duree_repousse:
                    self.carte[i][j] += 1
        
    def herbeMangee(self,i,j):
        if self.carte[i][j] >= self.duree_repousse :
            self.carte[i][j] = 0
        else :
            return False
    
    def nbHerbe(self) :
        cpt = 0
        for i in self.carte :
            for j in i :
                if j >= self.duree_repousse :
                    cpt += 1
        return cpt
                 
    def CoefCarte(self,i,j) :
        x = self.carte[i][j]
        return x


    

